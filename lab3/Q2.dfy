method Q2(x : int, y : int) returns (big : int, small : int)
  ensures big > small;
{
  if (x > y)
   {big, small := x, y;}
  else
   {big, small := y, x;}
}

/*

Q2 - Task 1
  x > y   x <= y

  Missing ensures, we do not cover the case where big = small


 Q2 - Task 2
  wp(S,R )
  S := if x > y then big := x ; small := y else big := y ;  small := x
  R := big > small

  Initial State:
    wp(if x > y then big:= x; small := y else big := y; small := x, big > small)

  By Conditional:
    (x >  y ) -> wp(big := x ; small := y, big > small ) &&
    (x <= y ) -> wp(big := y ; small := x, big > small )

  By Sequential:
    (x >  y ) -> wp(big := x, wp( small := y, big > small )) &&
    (x <= y ) -> wp(big := y, wp( small := x, big > small ))

  By Assignment:
    (x >  y ) -> wp(big := x, big > y ) &&
    (x <= y ) -> wp(big := y, big > x )

  By Assignment:
    (x >  y) -> (x > y ) &&
    (x <= y )-> (x > y )

  By Simplification:
    x >  y  -> true  &&
    x <= y  -> false

  By Simplification:
    true &&
    false

  End State:
    false

  Q2 - Task 3.1
  Add Requires x != y
  This changes the method result to strict inequality

  Q2 - Task 3.2
  Change Ensures Big >= Small
  This preserves the probable functionality of the method (that is to say x>y, x=y, x < y)

  Q2 - Task 3.3
  This makes it verifiable, however this is most likely not the
  intended behavior of the method


  method Q2(x : int, y : int) returns (big : int, small : int)
    ensures big > small;

    // Extra precise specification part.
    ensures old(x) == old(y) ==> small = old(y) - 1
  {
    if (x > y)
     {big, small := x, y;}
    else if ( x < y )
     {big, small := y, x;}
    else {
     {big, small := y + 1, x - 1;}
  }
  }
*/
