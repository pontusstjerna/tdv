method ComputeFact(n : nat) returns (res : nat)
requires n > 0;
ensures res == fact(n);
{
    res := 1;
    var i := 2;
    while (i <= n) 
    decreases n - i
    invariant i <= n + 1 && res * i == fact(i)
    {
        res := res * i;
        i := i + 1;
    }
}

function fact(n : nat) : nat
decreases n
{
    if n == 1 then 1 else if n == 0 then 1 else n * fact(n - 1)
}


/*

B := i <= n 
I := i <= n + 1 && res * i == fact(i)
D := n - i
S := res := res * i; i := i + 1;
R := res == fact(n)

-----------Initial-----------------------------------------

while (B I D S, R) =
    I
p0: && (i <= n && I ==> wp(res := res * i; i := i + 1;, I))
p1: && (i > n && I ==> res == fact(n))
p2: && (I ==> n - i >= 0)
p3: && (i <= n && I ==> wp(tmp := n - i; res := res * i; i := i + 1;, tmp > n - i))

#P0-----------Proof-of-p0----------------------------------
i <= n && I ==> wp(res := res * i; i := i + 1;, I)

i <= n && i <= n + 1 && res * i == fact(i) ==>
    wp(res := res * i; i := i + 1;, i <= n + 1 && res * i == fact(i))

--------------By-assignment-------------------------
i <= n && i <= n + 1 && res * i == fact(i) ==>
    i + 1 <= n + 1 && res * i * (i + 1) == fact(i + 1)

-------------By-arithmetic----i + 1 <= n + 1 == i <= n---------
i <= n && i <= n + 1 && res * i == fact(i) ==>
    i <= n && res * i * (i + 1) == fact(i + 1)

-------------By-Logic----A && B ==> A && C == B ==> C----------
i <= n + 1 && res * i == fact(i) ==>
    res * i * (i + 1) == fact(i + 1)

-------------By-definition-of-fact----------------------------
i <= n + 1 && res * i == fact(i) ==>
    res * i * (i + 1) == (i + 1) * fact(i + 1 - 1)

------------By-simplification------------------------------
i <= n + 1 && res * i == fact(i) ==>
    res * i * (i + 1) == (i + 1) * fact(i)

------------By-arithmetic----A * i == B * i == A == B------
i <= n + 1 && res * i == fact(i) ==>
    res * i == fact(i)

------------By-implication-rule----A && B ==> B == True----
p0: True




#P1----------Proof-of-p1---------------------------------
i > n && I ==> res == fact(n)

i > n && i <= n + 1 && res * i == fact(i) ==> res == fact(n)

-------------By-Logic----i <= n + 1 == i < n + 2-----------
i > n && i < n + 2 && res * i == fact(i) ==> res == fact(n)

-------------By-Logic----i > n && i < n + 2 == (i == n + 1)
i == n + 1 && res * i == fact(i) ==> res == fact(n)

-------------By-substitution----i == n + 1 == i - 1 == n---
i == n + 1 && res * i == fact(i) ==> res == fact(i - 1)

------------By-definition-of-fact-function-----------------
i == n + 1 && res * i == i * fact(i - 1) ==> res == fact(i - 1)

------------By-arithmetic----A * i == B * i == A == B------
i == n + 1 && res == fact(i - 1) ==> res == fact(i - 1)

------------By-implication-rule----A && B ==> B == True----
p1: True

            
#P2-----------Proof-of-p2----------------------------------
I ==> n - i >= 0

i <= n + 1 && res * i == fact(i)  ==> n - i >= 0

-----By-Arithmetic----n - i >= 0 == n >= i-----------------
i <= n + 1 && res * i == fact(i) ==> n >= i

----By-more-arithmetic--------n >= i == i <= n-------------
i <= n + 1 && res * i == fact(i) ==> i <= n

----By-Logic------i <= n == i <= n + 1---------------------
i <= n + 1 && res * i == fact(i) ==> i <= n + 1

----By-Logic--------A && B ==> A trivially true------------
p2: True


#P3----------Proof-of-p3-----------------------------------
i <= n && I ==> 
    wp(tmp := n - i; res := res * i; i := i + 1;, tmp > n - i)

i <= n && i <= n + 1 && res * i == fact(i) ==>
    wp(tmp := n - i; res := res * i; i := i + 1;, tmp > n - i)

-----By-Logic-----i <= n == i <= n + 1---------------------
i <= n + 1 && i <= n + 1 && res * i == fact(i) ==>
    wp(tmp := n - i; res := res * i; i := i + 1;, tmp > n - i)

----By-simplification----A && A == A-----------------------
i <= n + 1 && res * i == fact(i) ==>
    wp(tmp := n - i; res := res * i; i := i + 1;, tmp > n - i)

----By-Assignment------------------------------------------
i <= n + 1 && res * i == fact(i) ==>
    n - i > n - (i + 1)

----By-Arithmetic------------------------------------------
i <= n + 1 && res * i == fact(i) ==>
    n - i > n - i - 1

----By-more-arithmetic-------------------------------------
i <= n + 1 && res * i == fact(i) ==>
    0 > -1

----By trivial logic---------------------------------------
i <= n + 1 && res * i == fact(i) ==> true

----By-implication-rule-----A ==> true == true
p3: True

*/