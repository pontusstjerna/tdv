method Abs(x : int) returns (y : int)
  // return value doesn't deviate from intended value
  ensures 0 <= x ==> y == x;
  ensures x < 0 ==> y == -x;
   // return value is greater or equal to zero
  ensures 0 <= y;
{
  if (x < 0)
   { y := -x; }
  else
   { y := x; }
}

/*
Q1 - Task 1

Q -> wp(S,R)'
 '
S  := if x<0 then y = -x else y = x
R  := 0 <= x --> y == x && x < 0 --> y == -x && 0 <= y
B  := x < 0
!B := x >= 0
Initial state:
  wp(if x<0 then y = -x else y = x, 0 <= x --> y == x && x < 0 --> y == -x && 0 <= y)


Q1 - Task 2

By Conditional:
  (x <  0 -> wp(y = -x, 0 <= x --> y == x && x < 0 --> -x && 0 <= y)) &&
  (x >= 0 -> wp(y =  x, 0 <= x --> y == x && x < 0 --> -x && 0 <= y))

By Assignment:
  (x < 0  -> 0 <= x --> -x == x && x < 0 --> -x == -x && 0 <= -x) &&
  (x >= 0 -> 0 <= x -->  x == x && x < 0 -->  x == -x && 0 <=  x)

By Simplification:
  (x < 0  -> true  && x < 0 --> -x == -x && 0 <= -x) &&
  (x >= 0 -> 0 <= x -->  x == x && true  && 0 <=  x)

By Simplification:
  (x < 0  -> true -->  true && true) &&
  (x >= 0 -> true -->  true && true )

By Simplification:
  true -> true &&
  true -> true

End State:
  Q -> true
  (No precondition so leaving as Q)

 Q1 - Task 3
 As Abs does not modify anything it should just as well be a function.
 Had it modified x instead of sending a new variable, then method would've been
 more acceptable.
*/
