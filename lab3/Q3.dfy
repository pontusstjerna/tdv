method Q3(n0 : int, m0 : int) returns (res : int)
ensures res == n0 * m0
{
  var n, m : int;
  res := 0;
  if (n0 >= 0)
       {n,m := n0, m0;}
  else
       {n,m := -n0, -m0;}
  while (0 < n)
  decreases n
  invariant  0 <= n
  invariant n0 >= 0 ==> res == m *  (n0 - n) && m == m0
  invariant n0  < 0 ==> res == m * -(n0 + n) && m == -m0
  {
    res := res + m;
    n := n - 1;
  }
}

/*
----Q3-Task 1-------------------------------------------------------------------
n0, m0 are immutable, probably because they're cast to integer literals rather
than variables.

----Q3-Task 2-------------------------------------------------------------------
Multiplication of two Integers

ensures res == n0 * m0
decreases n
invariant 0 <= n
invariant n0 >= 0 ==> res == m *  (n0 - n) && m == m0
invariant n0  < 0 ==> res == m * -(n0 + n) && m == -m0

----Q3-Task 3-------------------------------------------------------------------
Index Table:
Initial State          - #IS
While Block            - #WB
Proof P0               - #P0
Proof P0 Finished      - #FP0
Proof P1               - #P1
Proof P1 Finished      - #FP1
Proof P2               - #P2
Proof P2 Finished      - #FP2
Proof P3               - #P3
Proof P3 Finished      - #FP3
State Recap            - #SR
Proof Then Branch      - #TB
Proof Else Branch      - #EB

------Initial State-------------------------------------------------------------
wp (res = 0 ;if n0 >= 0
             then n:= n0; m := m0
             else n := -n0 ; m := -m0;
             while 0 < n res := res + m; n := n - 1, I)

------By-Sequential-------------------------------------------------------------
wp (res = 0, wp(if n0 >= 0
                then n:= n0; m := m0
                else n := -n0 ; m := -m0;
                while 0 < n res := res + m; n := n - 1, I))

wp (res = 0, wp(if n0 >= 0
                then n:= n0; m := m0
                else n := -n0 ; m := -m0,
                wp(while 0 < n res := res + m; n := n - 1, I))

-------Proof-of-While-Block-----------------------------------------------------
Reference Sheet [RS]:
B  :=   0 < n
I  :=   0 <= n && n0 >= 0 ==> (res == m *  (n0 - n) && m == m0)
               && n0  < 0 ==> (res == m * -(n0 + n) && m == -m0)
D  :=   n
S  :=   res := res + m;  n := n - 1
R  :=   res == n0 * m0

wp(while 0 < n res := res + m; n := n - 1, I)) =

wp(B I D S, R) =
  I
p0: && (0 < n && I) ==> wp(res:= res + m; n:= n - 1, I)
p1: && (!(0 < n) && I) ==> res == n0 * m0
p2: && I ==> (n >= 0)
p3: && (0 < n && I) ==> wp(tmp := D; res := res + m;  n := n - 1, tmp > D)

#P0-----Proof-of-p0-------------------------------------------------------------
(0 < n && I) ==> wp(res:= res + m; n:= n - 1, I)

--------By-Sequential-----------------------------------------------------------
(0 < n && I) ==> wp(res:= res + m, wp(n:= n - 1, I))

--------By-Assignment-----------------------------------------------------------
wp(n:= n - 1, 0 <= n && n0 >= 0 ==> (res == m *  (n0 - n) && m == m0)
                     && n0  < 0 ==> (res == m * -(n0 + n) && m == -m0)) =

0 <= (n-1) && n0 >= 0 ==> (res == m *  (n0 - (n-1) ) && m == m0)
           && n0  < 0 ==> (res == m * -(n0 + (n-1) ) && m == -m0) =

wp(res:= res + m,
    0 <= (n-1) && n0 >= 0 ==> (res == m *  (n0 - (n-1) ) && m == m0)
              && n0  < 0 ==> (res == m * -(n0 + (n-1) ) && m == -m0)) =

0 <= (n-1) && n0 >= 0 ==> (res + m == m *  (n0 - (n-1) ) && m == m0)
           && n0  < 0 ==> (res + m == m * -(n0 + (n-1) ) && m == -m0)

--------By-Arithmetic---(a-(b-c) == (a-b+c)---(a+(b-c)) == (a+b-c)---------------
(0 < n && 0 <= n && n0 >= 0 ==> (res == m *  (n0 - n) && m == m0)
                 && n0  < 0 ==> (res == m * -(n0 + n) && m == -m0))
                 ==>
      0 <= (n-1) && n0 >= 0 ==> (res + m == m *  (n0 - n + 1 ) && m == m0)
                 && n0  < 0 ==> (res + m == m * -(n0 + n - 1 ) && m == -m0)

--------By-Arithmetic--- (-m) + m == 0------------------------------------------
(0 < n && 0 <= n && n0 >= 0 ==> (res == m *  (n0 - n) && m == m0)
                 && n0  < 0 ==> (res == m * -(n0 + n) && m == -m0))
                 ==>
      0 <= (n-1) && n0 >= 0 ==> (res == m *  (n0 - n) && m == m0)
                 && n0  < 0 ==> (res == m * -(n0 + n) && m == -m

--------By-Logic---A ==> A----0 < n && 0 <= n == 0 < n--------------------------
0 < n  && n0 >= 0 ==> (res == m *  (n0 - n) && m == m0)
       && n0  < 0 ==> (res == m * -(n0 + n) && m == -m0))
       ==>
  0 <= (n-1) && True
             && True

--------By-Logic--- 0 <= n True, by 0 < n --------------------------------------
0 < n  && n0 >= 0 ==> (res == m *  (n0 - n) && m == m0)
       && n0  < 0 ==> (res == m * -(n0 + n) && m == -m0))
       ==>
  True && True && True

--------By-Logic---- a ==> True, True ------------------------------------------
True ==> True =
True

-------Proof-of-p0-Finished-#FP0---------------------------------------------------
pO: True

-------Proof-of-#p1--------------------------------------------------------------
(!(0 < n) && I) ==> res == n0 * m0

--------By-Logic--- Negation < == >= ----- Unfold Definiton of I ---------------
(0 >= n && I) ==> res == n0 * m0

(0 >= n) && 0 <= n
          && n0 >= 0 ==> (res == m *  (n0 - n) && m == m0)
          && n0  < 0 ==> (res == m * -(n0 + n) && m == -m0))
          ==> res == n0 * m0

--------By-Logic--- 0 <= n && 0 >= n ==> n==0 ----------------------------------
n==0 && n0 >= 0 ==> (res == m *  (n0 - n) && m == m0)
     && n0  < 0 ==> (res == m * -(n0 + n) && m == -m0))
     ==> res == n0 * m0

--------By-Logic--- n==0 ==> (n0 +-0) ------------------------------------------
n==0 && n0 >= 0 ==> (res == m0 *  n0  && m == m0)
     && n0  < 0 ==> (res == -m0 * -n0 && m == -m0))
     ==> res == n0 * m0

--------By-Logic--- n0 < 0 Branch statement cannot be true by n==0--------------
n==0 && n0 >= 0 ==> (res == m0 *  n0  && m == m0) && False
     ==> res == n0 * m0

--------By-Logic--- Trivial Truth a ==> a---------------------------------------
n==0 && n0 >= 0 ==> (res == m0 *  n0  && m == m0) && False
     ==> True

-------Proof-of-p1-Finished-----------------------------------------------------
p1: True

-------Proof-of-#p2--------------------------------------------------------------
I ==> (n >= 0) =
0 <= n && n0 >= 0 ==> (res == m *  (n0 - n) && m == m0)
       && n0  < 0 ==> (res == m * -(n0 + n) && m == -m0)
       ==> (n >= 0)

--------By-Arithmetic---- 0 <= n == n >= 0 -------------------------------------
n >= 0 && n0 >= 0 ==> (res == m *  (n0 - n) && m == m0)
       && n0  < 0 ==> (res == m * -(n0 + n) && m == -m0)
       ==> (n >= 0)

--------By-Logic--- Trivial Truth a ==> a---------------------------------------
n >= 0 && n0 >= 0 ==> (res == m *  (n0 - n) && m == m0)
       && n0  < 0 ==> (res == m * -(n0 + n) && m == -m0)
       ==> True

-------Proof-of-p1-Finished-----------------------------------------------------
p2: True

-------Proof-of-#p2--------------------------------------------------------------
(0 < n && I) ==> wp(tmp := n; res := res + m;  n := n - 1, tmp > n)

--------By-Sequential-----------------------------------------------------------
wp(tmp := n; res := res + m;  n := n - 1, tmp > n) =
wp(tmp := n, wp( res := res + m;  n := n - 1, tmp > n)) =
wp(tmp := n, wp( res := res + m,  wp(n := n - 1, tmp > n))) =

--------By-Assignment-----------------------------------------------------------
wp(n := n - 1, tmp > n) =
tmp > n - 1

wp( res := res + m, tmp > n - 1) =
tmp > n - 1

wp(tmp := n, tmp > n - 1) =
n > n - 1

--------By-Logic--- n > n - 1 Trivially True------------------------------------
(0 < n && I) ==> True

-------Proof-of-p2-Finished-----------------------------------------------------
p2: True


-------Proof-of-While-Block-Finished--------------------------------------------
While(B I D S, R ) = I && True && True && True && True = I


-------State-Recap--#SR---------------------------------------------------------
wp (res = 0, wp(if n0 >= 0
                then n:= n0; m := m0
                else n := -n0 ; m := -m0,
                wp(while 0 < n res := res + m; n := n - 1, I)) =

wp (res = 0, wp(if n0 >= 0
                then n:= n0; m := m0
                else n := -n0 ; m := -m0 , I)

-------By-Conditional-----------------------------------------------------------
wp(if n0 >= 0 then n:= n0; m := m0 else n := -n0 ; m := -m0 , I) =
  (n0 >= 0 => wp(n:= n0; m := m0, I)) &&
  (!(n0>=0) ==> wp(n := -n0 ; m := -m0 , I))

--------Proof-of-then-Branch--#TB-----------------------------------------------
n0 >= 0 => wp(res :=0, wp(n:= n0; m := m0, I))

---------By-Sequential---------------------res added for simplicity-------------
n0 >= 0 => wp(res:= 0, wp(n:= n0, wp( m := m0, I)))

---------By-Assignment----------------------------------------------------------
wp( m := m0, 0 <= n && n0 >= 0 ==> (res == m *  (n0 - n) && m == m0)
                    && n0  < 0 ==> (res == m * -(n0 + n) && m == -m0))=

0 <= n && n0 >= 0 ==> (res == m0 *  (n0 - n) && m0 == m0)
       && n0  < 0 ==> (res == m0 * -(n0 + n) && m0 == -m0)

wp(n:= n0, 0 <= n && n0 >= 0 ==> (res == m0 *  (n0 - n) && m0 == m0)
                  && n0  < 0 ==> (res == m0 * -(n0 + n) && m0 == -m0)) =

0 <= n0 && n0 >= 0 ==> (res == m0 *  (n0 - n0) && m0 == m0)
        && n0  < 0 ==> (res == m0 * -(n0 + n0) && m0 == -m0) =

wp(res :=0, 0 <= n0 && n0 >= 0 ==> (res == m0 *  (n0 - n0) && m0 == m0)
                    && n0  < 0 ==> (res == m0 * -(n0 + n0) && m0 == -m0)) =

0 <= n0 && n0 >= 0 ==> (0 == m0 *  (n0 - n0) && m0 == m0)
        && n0  < 0 ==> (0 == m0 * -(n0 + n0) && m0 == -m0)

Thus:
(n0 >= 0) ==> 0 <= n0 && n0 >= 0 ==> (0 == m0 *  (n0 - n0) && m0 == m0)
                      && n0  < 0 ==> (0 == m0 * -(n0 + n0) && m0 == -m0)

-------By-Logic--- 0 <= n && n >= 0 ==> n == 0----- a == a ==> True-------------
(n0 >= 0) ==> 0 <= n0 && n0 >= 0 ==> (0 == m0 *  (n0 - n0) && True)
                      && n0  < 0 ==> (0 == m0 * -(n0 + n0) && m0 == -m0)


-------By-Arithmetic--- a-a = 0 --- a + a = 2a ---------------------------------
(n0 >= 0) ==> 0 <= n0 && n0 >= 0 ==> (0 == m0 * 0 && True)
                      && n0  < 0 ==> (0 == m0 * -2n0 && m0 == -m0)

-------By-Logic--- 0 * a == 0 ==> True -----------------------------------------
(n0 >= 0) ==> 0 <= n0
              && n0 >= 0 ==> (True && True)
              && n0  < 0 ==> (0 == m0 * -2n0 && m0 == -m0)

(n0 >= 0) ==> n0 >= 0 && True
              && n0  < 0 ==> (0 == m0 * -2n0 && m0 == -m0)

-------By-Logic--- n0>= 0 ==> n0 <0 ==> True ==> False--------------------------
(n0 >= 0) ==> n0 >= 0 && True
              && False ==> (0 == m0 * -2n0 && m0 == -m0)

-------By-Logic--- False ==> Anything == True-----------------------------------
(n0 >= 0) ==> n0 >= 0 && True && True

--------Proof-of-then-Branch-Finished-#TBF--------------------------------------
(n0 >= 0) ==> True && True && True == True

--------Proof-of-else-Branch-#EB------------------------------------------------
(!(n0>=0) ==> wp(n := -n0 ; m := -m0 , I))

---------By-Sequential---------------------res added for simplicity-------------
(!(n0>=0) ==> wp( res:= 0, wp(n := -n0,  wp(m := -m0 , I)))

---------By-Assignment----------------------------------------------------------
wp(m := -m0 , 0 <= n && n0 >= 0 ==> (res == m *  (n0 - n) && m == m0)
                     && n0  < 0 ==> (res == m * -(n0 + n) && m == -m0)) =

0 <= n && n0 >= 0 ==> (res == -m0 *  (n0 - n) && -m0 == m0)
       && n0  < 0 ==> (res == -m0 * -(n0 + n) && -m0 == -m0) =

wp(n := -n0, 0 <= n && n0 >= 0 ==> (res == -m0 *  (n0 - n) && -m0 == m0)
                    && n0  < 0 ==> (res == -m0 * -(n0 + n) && -m0 == -m0)) =

0 <= -n0 && n0 >= 0 ==> (res == -m0 *  (n0 - (-n0)) && -m0 == m0)
        && n0  < 0 ==> (res == -m0 * -(n0 + (-n0)) && -m0 == -m0) =

wp( res:= 0,0 <= -n0 && n0 >= 0 ==> (res == -m0 *  (n0 - (-n0)) && -m0 == m0)
                    && n0  < 0 ==> (res == -m0 * -(n0 + (-n0)) && -m0 == -m0))=

0 <= -n0 && n0 >= 0 ==> (0 == -m0 *  (n0 - (-n0)) && -m0 == m0)
         && n0  < 0 ==> (0 == -m0 * -(n0 + (-n0)) && -m0 == -m0)

Thus:
(!(n0>=0) ==> 0 <= -n0
              && n0 >= 0 ==> (0 == -m0 *  (n0 - (-n0)) && -m0 == m0)
              && n0  < 0 ==> (0 == -m0 * -(n0 + (-n0)) && -m0 == -m0)

---------By-Logic--- Negation of n0 >= 0----------------------------------------
0 < n0 ==> 0 <= -n0
           && n0 >= 0 ==> (0 == -m0 *  (n0 - (-n0)) && -m0 == m0)
           && n0  < 0 ==> (0 == -m0 * -(n0 + (-n0)) && -m0 == -m0)

---------By-Arithmetic--- (n -(-n)) == 2n --- n + (-n) == 0---------------------
0 < n0 ==> 0 <= -n0
           && n0 >= 0 ==> (0 == -m0 * 2n0 && -m0 == m0)
           && n0  < 0 ==> (0 == -m0 *0    && -m0 == -m0)

0 < n0 ==> 0 <= -n0
           && n0 >= 0 ==> (0 == -m0 * 2n0 && -m0 == m0)
          && n0  < 0 ==> (0 == 0   && -m0 == -m0)

---------By-Logic--- 0 == 0 && -m0 == -m0 Trivially True------------------------
0 < n0 ==> 0 <= -n0
           && n0 >= 0 ==> (0 == -m0 * 2n0 && -m0 == m0)
          && n0  < 0 ==> True

0 < n0 ==> 0 <= -n0
           && n0 >= 0 ==> (0 == -m0 * 2n0 && -m0 == m0)
           && True

---------By-Logic--- 0 < n0 ==> n>=0 == True ==> False--------------------------
0 < n0 ==> 0 <= -n0
           && False ==> (0 == -m0 * 2n0 && -m0 == m0)
           && True

0 < n0 ==> 0 <= -n0  && True && True

.---------By-Arithmetic--- 0 < n0 == 0*(-1) < (-1)*(-1)*0-----------------------
0 < n0 ==> 0 <= n0  && True && True

--------Proof-of-else-Branch-Finished-#EB---------------------------------------
0 < n0 ==> True && True && True =
True

--------End-of Proof------------------------------------------------------------
By both branches we have true && true. 
Thus we have proved the entirety to be true.

Do note that we used a shortcut for writing here as in reality the final
assignment should have been taken care of in this form:

wp(res := 0, Boolean Expression for IFTHENELSE) and that would have been
unnecessarily large to write out.
