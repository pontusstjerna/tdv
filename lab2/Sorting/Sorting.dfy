class Sorting {
    
    predicate sorted(list: seq<int>)
    {
        forall x :: 0 <= x < |list| - 1 ==> list[x] <= list[x + 1]
    }

    predicate sorted'(list: seq<int>) 
    {
        forall x,y :: 0 <= x < y < |list| ==> list[x] <= list[y]
    }

// if a sequence is sorted', then it is sorted
    ghost method SortEquivalence(list : seq<int>)
    requires sorted'(list)
    ensures sorted(list)
    {}

// if a sequence is sorted, then it is sorted'
    // ghost method SortEquivalence'(list: seq<int>) 
    // requires sorted(list)
    // ensures sorted'(list)
    // {}

    predicate p(a : seq<int>, b : seq<int>) 
    requires |a| == |b|
    {
        forall x :: 0 <= x < |a| ==> occurences(a[x], a) == occurences(a[x], b)
    }

    function occurences(elem : int, sequence : seq<int>) : int
    {
        if |sequence| == 0 then 0
        else
            if sequence[0] == elem then 
                occurences(elem, sequence[1..]) + 1
            else
                occurences(elem, sequence[1..])
    }

    // method superSort(arr : array<int>)
    // modifies arr
    // requires arr != null
    // ensures sorted(arr[..])
    // ensures p(old(arr[..]), arr[..])
    // {
    //     print "Super sorting!";
    // }

    method superSortUseless(arr : array<int>)
    modifies arr
    requires arr != null
    ensures sorted(arr[..])
    //ensures p(old(arr[..]), arr[..])
    {
        var i := 0;
        while (i < arr.Length) 
            invariant 0 <= i <= arr.Length
            invariant forall x :: 0 <= x < i ==> arr[x] == 0
        {
            arr[i] := 0;
            i := i + 1;
        }
    }

    method Main() {
        var sortedList := [1,2,3,4,4];
        assert sorted(sortedList);
        assert sorted'(sortedList);
        assert p([2,1,2], [1,2,2]);
    }
}