method Main() 
{
    print "Dis is main.";
}

class Token 
{   
    var identifier : int;
    var clearance : int; // 0 = low, 1 = medium, 2 = high
    var isValid : bool;


    method Init(id : int, clearanceLevel : int) 
    modifies this, this`identifier, this`clearance, this`isValid
    requires id >= 0
    requires 0 <= clearanceLevel <= 2
    ensures Valid()
    ensures identifier == id
    ensures clearance == clearanceLevel
    {
        identifier := id;
        clearance := clearanceLevel;
        isValid := true;
    }  

    predicate Valid()
    reads this
    {
        identifier >= 0 &&
        0 <= clearance <= 2
    }
}

class IDStation
{
    var securityLevel : int;
    var isOpen : bool;
    var alarmActive : bool;

    predicate Valid() 
    reads this
    {
        0 <= securityLevel <= 2
    }

    method Init(securityLevel : int) 
    modifies this, this`securityLevel, this`isOpen, this`alarmActive
    requires 0 <= securityLevel <= 2
    ensures Valid() 
    {
        this.securityLevel := securityLevel;
        isOpen := false;
        alarmActive := false;
    }

    method EnterDoor(id: int, token: Token) 
    modifies this, this`isOpen, this`alarmActive, token
    requires Valid()
    requires !isOpen
    requires token != null
    requires token.Valid()
    ensures Valid()
    ensures isOpen || !isOpen
    ensures token.identifier == id && token.clearance >= securityLevel ==> isOpen
    ensures token.identifier != id ==> alarmActive
    {
        if (token.identifier != id) 
        {
            alarmActive := true;
            isOpen := false;
            token.isValid := false;
        } 
        else 
        {
            if (token.clearance >= securityLevel) 
            {
                isOpen := true;
            } 
        }
    }

    method InvalidateToken(token : Token) 
    modifies this, this`alarmActive, token
    requires token != null
    requires token.Valid()
    requires !isOpen
    ensures !token.isValid
    {  
        token.isValid := false;
        alarmActive := false;
    }

    method CloseDoor() 
    modifies this`isOpen
    requires isOpen
    requires Valid()
    ensures !isOpen
    {
        isOpen := false;
    }
}

class EnrollmentStation
{
    var users : map<int, Token>

    predicate Valid()
    reads this, * // Is there another way?
    {
        // No two or more tokens belong to the same user
        forall x,y :: x in users && y in users ==> x != y ==> users[x] != null && users[y] != null ==> users[x].identifier != users[y].identifier
    }

    method Init()
    modifies this;
    ensures Valid()
    {
        users := map[];
    }

    method IssueToken(id : int, securityLevel : int) 
    modifies this`users
    requires Valid()
    requires id >= 0
    requires 0 <= securityLevel <= 2
    requires !(id in users) // Make sure the id is not present as key
    ensures id in users
    ensures fresh(users[id])
    ensures users[id] != null
    ensures users[id].identifier == id
    ensures users[id].clearance == securityLevel
    {
          if (!(id in users)) 
          {
            var newToken := new Token;
            newToken.Init(id, securityLevel);
            users := users[id := newToken];       
          } 
    }
}