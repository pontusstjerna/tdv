// A LIFO queue (aka a stack) with limited capacity.
class LimitedStack{

      var capacity : int; // capacity, max number of elements allowed on the stack.
      var arr : array<int>; // contents of stack.
      var top : int; // The index of the top of the stack, or -1 if the stack is empty

      // This predicate express a class invariant: All objects of this calls should satisfy this.
      predicate Valid()
      reads this, this.arr;
      {
        arr != null            &&
        capacity > 0           &&
        arr.Length == capacity &&
        top >= -1              &&
        top < capacity

      }

      predicate Usable()
      reads this, this.arr;
      {
        !Empty() &&
        top > -1 &&
        Valid()
      }

      predicate Empty()
      reads this;
      {
        top == -1
      }

      predicate Full()
      reads this;
      {
        top + 1 == capacity
      }


      method Init(c : int)
      modifies this;
      requires c > 0

      ensures fresh(arr); // ensures arr is a newly created object.
      ensures Valid();
      ensures Empty();
      ensures capacity == c
      // Additional post-condition to be given here!
      {
        capacity := c;
        arr := new int[c];
        top := -1;
      }

      function method isEmpty(): bool
      reads this, this.arr
      requires Valid()
      {
        top == -1
      }

      function method Peek(): int
      reads this, this.arr
      requires   !Empty()
      requires  top > -1 && top < capacity
      requires  Valid()
      {
        arr[top]
      }

      method Pop() returns (elem : int)
      modifies this.arr, this`top;
      requires !Empty()
      requires Valid()
      ensures top == old(top - 1)
      ensures elem == old(arr[top])
      ensures old(top == 0) ==> top == -1
      ensures forall i :: 0 <= i <= top ==> arr[i] == old(arr[i])
      {
        elem := arr[top];
        top  := top - 1;
      }

      method Push(e : int )
      modifies this.arr, this`top;
      requires !Full()
      requires Valid()
      ensures top == old(top + 1);
      ensures arr[old(top + 1)] == e;
      ensures forall i :: 0 <= i <= old(top) ==> arr[i] == old(arr[i])
      {
        top := top + 1 ;
        arr[top] := e;

      }

           method Shift()
           requires Valid() && !Empty();
           ensures Valid();
           ensures forall i : int :: 0 <= i < capacity - 1 ==> arr[i] == old(arr[i + 1]);
           ensures top == old(top) - 1;
           modifies this.arr, this`top;
           {
             var i : int := 0;
             while (i < capacity - 1 )
             invariant 0 <= i < capacity;
             invariant top == old(top);
             invariant forall j : int :: 0 <= j < i ==> arr[j] == old(arr[j + 1]);
             invariant forall j : int :: i <= j < capacity ==> arr[j] == old(arr[j]);
             {
               arr[i] := arr[i + 1];
               i := i + 1;
             }
             top := top - 1;
           }


           //Push onto full stack, oldest element is discarded.
           method Push2(elem : int)
           modifies this.arr, this`top
           requires Valid()
           ensures (top < capacity && old(top) + 1 != capacity) ==> (top  == old(top) + 1 && arr[top] == elem);
           ensures (old(top) + 1 == capacity) ==> top == old(top)  && arr[top] == elem
           ensures (old(top) + 1 == capacity) ==>(forall i : int :: 0 <= i < capacity - 1 ==> arr[i] == old(arr[i + 1]))
           {
             if (top + 1 == capacity) {
                Shift();
             }

             Push(elem);
           }




     // When you are finished,  all the below assertions should be provable.
     // Feel free to add extra ones as well.
           method Main(){
                var s := new LimitedStack;
                s.Init(3);

                assert s.Empty() && !s.Full();

                s.Push(27);
                assert !s.Empty();

                var e := s.Pop();
                assert e == 27;

                s.Push(5);
                assert s.arr[0] == 5;
                s.Push(32);
                s.Push(9);
                assert s.Full();
                var e2 := s.Pop();
                assert s.top == 1;
                assert e2 == 9;
                assert !s.Full();
                assert s.arr[0] == 5;

                s.Push(e2);
                s.Push2(99);

                var e3 := s.Peek();
                assert e3 == 99;
                assert s.arr[0] == 32;

            }

     }