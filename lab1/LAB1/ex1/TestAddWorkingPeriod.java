import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertTrue;

public class TestAddWorkingPeriod {

    @Test
    public void test_addWorkingPeriod_part1() throws Exception {
        int startTime = -155, endTime = 0;
        int size = 1;

        String z = "1viogjhtfio_0";


        WorkSchedule pre = new WorkSchedule(size);
        WorkSchedule post = new WorkSchedule(size);

        assertEqual(pre, post, size); // Assert they are equal before method call
        assertTrue(!post.addWorkingPeriod(z, startTime, endTime));
        assertEqual(pre, post, size); // Assert they are equal after method call
    }

    @Test
    public void test_addWorkingPeriod_part2() throws Exception {
        int startTime = 50;
        int endTime = 75;
        int size = 90;

        String z = "öijianwdiunawd";

        WorkSchedule pre = new WorkSchedule(size);
        WorkSchedule post = new WorkSchedule(size);

        int requiredNumber = post.readSchedule(startTime).requiredNumber;

        assertEqual(pre, post, size); // Assert they are equal before method call
        assertTrue(!post.addWorkingPeriod(z, startTime, endTime) &&
                post.workingEmployees(startTime, endTime).length == requiredNumber); // What about workingEmployees? Not yet tested
        assertEqual(pre, post, size); // Assert they are equal after method call
    }

    @Test
    public void test_addWorkingPeriod_part3() throws Exception {
        int startTime = 50;
        int endTime   = 75;
        int size      = 90;

        String employee = "ngn";

        WorkSchedule pre  = new WorkSchedule(size);
        WorkSchedule post = new WorkSchedule(size);

        post.setRequiredNumber(2, startTime, endTime);
        pre.setRequiredNumber(2, startTime, endTime);

        post.addWorkingPeriod(employee, startTime, endTime);
        pre.addWorkingPeriod(employee, startTime, endTime);

        int requiredNumber = post.readSchedule(startTime).requiredNumber;

        assertEqual(pre, post, size); // Assert they are equal before method call
        assertTrue(requiredNumber != post.workingEmployees(startTime, endTime).length &&
                !post.addWorkingPeriod(employee, startTime, endTime));
        assertEqual(pre, post, size); // Assert they are equal after method call
    }

    @Test
    public void test_addWorkingPeriod_part4() throws Exception {
        int startTime = 50;
        int endTime = 75;
        int size = 90;


        String employee = "ngn";

        WorkSchedule pre = new WorkSchedule(size);
        WorkSchedule post = new WorkSchedule(size);

        post.setRequiredNumber(1, startTime, endTime);
        pre.setRequiredNumber(1, startTime, endTime);

        int requiredNumber = post.readSchedule(startTime).requiredNumber;

        assertEqual(pre, post, size); // Assert they are equal before method call
        assertTrue(requiredNumber != post.workingEmployees(startTime, endTime).length &&
                post.addWorkingPeriod(employee, startTime, endTime));

        //Unsure since specs say startTime < endTime, and assignment happens on endTime.
        for (int i = startTime; i <= endTime; i++) {
            assertTrue(Arrays.stream(post.readSchedule(i).workingEmployees).anyMatch((x) -> x.equals(employee)));
        }

        // Will fail if asserted with endTime + 0, since an employee is assigned to the actual endTime hour
        assertEqual(pre, post, size, endTime + 1);
    }

    @Test
    public void test_addWorkingPeriod_border1() throws Exception {
        int size = 1;
        int startTime = Integer.MIN_VALUE, endTime = size;

        String z = "1viogjhtfio_0";


        WorkSchedule pre = new WorkSchedule(size);
        WorkSchedule post = new WorkSchedule(size);

        assertEqual(pre, post, size); // Assert they are equal before method call
        assertTrue(!post.addWorkingPeriod(z, startTime, endTime));
        assertEqual(pre, post, size); // Assert they are equal after method call
    }

    @Test
    public void test_addWorkingPeriod_border2() throws Exception {
        int size = 1;
        int startTime = -1, endTime = size;

        String z = "1viogjhtfio_0";


        WorkSchedule pre = new WorkSchedule(size);
        WorkSchedule post = new WorkSchedule(size);

        assertEqual(pre, post, size); // Assert they are equal before method call
        assertTrue(!post.addWorkingPeriod(z, startTime, endTime));
        assertEqual(pre, post, size); // Assert they are equal after method call
    }

    @Test
    public void test_addWorkingPeriod_border3() throws Exception {
        int size = 1;
        int startTime = Integer.MIN_VALUE, endTime = Integer.MAX_VALUE;

        String z = "1viogjhtfio_0";


        WorkSchedule pre = new WorkSchedule(size);
        WorkSchedule post = new WorkSchedule(size);

        assertEqual(pre, post, size); // Assert they are equal before method call
        assertTrue(!post.addWorkingPeriod(z, startTime, endTime));
        assertEqual(pre, post, size); // Assert they are equal after method call
    }

    @Test
    public void test_addWorkingPeriod_border4() throws Exception {
        int size = 1;
        int startTime = -1, endTime = Integer.MAX_VALUE;

        String z = "1viogjhtfio_0";


        WorkSchedule pre = new WorkSchedule(size);
        WorkSchedule post = new WorkSchedule(size);

        assertEqual(pre, post, size); // Assert they are equal before method call
        assertTrue(!post.addWorkingPeriod(z, startTime, endTime));
        assertEqual(pre, post, size); // Assert they are equal after method call
    }

    private void assertEqual(WorkSchedule preWs, WorkSchedule postWs, int size) {
        assertEqual(preWs, postWs, size, 0);
    }

    public static void assertEqual(WorkSchedule preWs, WorkSchedule postWs, int size, int startTime ) {
        WorkSchedule.Hour pre, post;

        for(int i = startTime; i < size; i++) {
            pre = preWs.readSchedule(i);
            post = postWs.readSchedule(i);

            assertTrue(pre.requiredNumber == post.requiredNumber);
            assertTrue(Arrays.deepEquals(pre.workingEmployees, post.workingEmployees));
        }
    }
}