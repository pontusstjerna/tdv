import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class TestWorkingEmployeeees {
    @Test
    public void test_workingEmployees_part1() throws Exception {
        int size = 5;
        int startTime = 1;
        int endTime = 4;
        WorkSchedule pre = new WorkSchedule(size);
        WorkSchedule post = new WorkSchedule(size);

        // Assert the schedule is unchanged (before calling method)
        TestAddWorkingPeriod.assertEqual(pre, post, size, startTime);

        pre.setRequiredNumber(1, startTime, endTime);
        post.setRequiredNumber(1,startTime,endTime);

        pre.addWorkingPeriod("Null", startTime, endTime);
        post.addWorkingPeriod("Null", startTime, endTime);

        String[] result = post.workingEmployees(startTime, endTime);

        // Test that the length of all distinct elements in resulting array is equal
        // to the length of the original array
        assertTrue(Arrays.stream(result).distinct().count() == result.length);

       
        //List to keep track of all the Strings for a given start-endtime interval.
        List<String> employeesInInterval = new ArrayList<>();
        
        //Add every string in workingEmployees to the List for each given hour in the interval.
        for (int i = startTime; i <= endTime; i++) {
            Collections.addAll(employeesInInterval, post.readSchedule(i).workingEmployees);
        }

        //A string appears in the return array if and only if
        //it appears in the workingEmployees of at least one hour in the interval starttime to endtime
        //As such filter the result array with the list of all strings in the interval, check if the count is greater
        //or equal to the length of the result array.
        assertTrue(Arrays.stream(result).filter(employeesInInterval::contains).count() >= result.length);





        // Assert the schedule is unchanged
        TestAddWorkingPeriod.assertEqual(pre, post, size, startTime);
    }

    @Test
    public void test_workingEmployees_part2() throws Exception {
        int size = 4;
        int startTime = 2;
        int endTime = 0;
        WorkSchedule pre = new WorkSchedule(size);
        WorkSchedule post = new WorkSchedule(size);

        // Assert the schedule is unchanged (before calling method)
        TestAddWorkingPeriod.assertEqual(pre, post, size, startTime);

        pre.setRequiredNumber(1, startTime, endTime);
        post.setRequiredNumber(1,startTime,endTime);

        pre.addWorkingPeriod("Null", startTime, endTime);
        post.addWorkingPeriod("Null", startTime, endTime);

        assertTrue(post.workingEmployees(startTime, endTime).length == 0);

        // Assert the schedule is unchanged
        TestAddWorkingPeriod.assertEqual(pre, post, size, startTime);
    }

    @Test
    public void testWorkingEmployees_border1() throws Exception {
        int size = Integer.MAX_VALUE;
        int startTime = 0;
        int endTime = size - 1;
        WorkSchedule pre = new WorkSchedule(size);
        WorkSchedule post = new WorkSchedule(size);

        TestAddWorkingPeriod.assertEqual(pre, post, size, startTime);

        pre.setRequiredNumber(1, startTime, endTime);
        post.setRequiredNumber(1,startTime,endTime);

        pre.addWorkingPeriod("Null", startTime, endTime);
        post.addWorkingPeriod("Null", startTime, endTime);

        String[] result = post.workingEmployees(startTime, endTime);

        assertTrue(Arrays.stream(result).distinct().count() == result.length);

        List<String> employeesInInterval = new ArrayList<>();

        for (int i = startTime; i <= endTime; i++) {
            Collections.addAll(employeesInInterval, post.readSchedule(i).workingEmployees);
        }

        assertTrue(Arrays.stream(result).filter(employeesInInterval::contains).count() >= result.length);

        TestAddWorkingPeriod.assertEqual(pre, post, size, startTime);
    }

    @Test
    public void testWorkingEmployees_border2() throws Exception {
        int size = 4;
        int startTime = Integer.MAX_VALUE;
        int endTime = Integer.MIN_VALUE;
        WorkSchedule pre = new WorkSchedule(size);
        WorkSchedule post = new WorkSchedule(size);

        TestAddWorkingPeriod.assertEqual(pre, post, size, startTime);

        pre.setRequiredNumber(1, startTime, endTime);
        post.setRequiredNumber(1,startTime,endTime);

        pre.addWorkingPeriod("Null", startTime, endTime);
        post.addWorkingPeriod("Null", startTime, endTime);

        assertTrue(post.workingEmployees(startTime, endTime).length == 0);

        TestAddWorkingPeriod.assertEqual(pre, post, size, startTime);
    }



    @Test
    public void testWorkingEmployees_border3() throws Exception {
        int size = 4;
        int startTime = 0;
        int endTime = 0;
        WorkSchedule pre = new WorkSchedule(size);
        WorkSchedule post = new WorkSchedule(size);

        TestAddWorkingPeriod.assertEqual(pre, post, size, startTime);

        pre.setRequiredNumber(1, startTime, endTime);
        post.setRequiredNumber(1,startTime,endTime);

        pre.addWorkingPeriod("Null", startTime, endTime);
        post.addWorkingPeriod("Null", startTime, endTime);

        String[] result = post.workingEmployees(startTime, endTime);

        assertTrue(Arrays.stream(result).distinct().count() == result.length);

        List<String> employeesInInterval = new ArrayList<>();

        for (int i = startTime; i <= endTime; i++) {
            Collections.addAll(employeesInInterval, post.readSchedule(i).workingEmployees);
        }

        assertTrue(Arrays.stream(result).filter(employeesInInterval::contains).count() >= result.length);

        TestAddWorkingPeriod.assertEqual(pre, post, size, startTime);
    }

}
