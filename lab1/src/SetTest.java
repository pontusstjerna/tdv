import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

/**
 * Created by ponstj on 11/20/17.
 */
public class SetTest {
    @Test
    public void insert() throws Exception {

        /* STATEMENT AND BRANCH COVERAGE */
        Set set = new Set();
        set.insert(1);
        set.insert(0);
        set.insert(1);

        assertTrue(Arrays.equals(set.toArray(), new int[]{0, 1}));
    }

    @Test
    public void member() throws Exception {

        /* STATEMENT AND BRANCH COVERAGE */
        Set set = new Set();
        assertTrue(!set.member(0));
        set.insert(1);
        assertTrue(!set.member(0));
        assertTrue(set.member(1));
        assertTrue(!set.member(2));
    }

    @Test
    public void section() throws Exception {

        /* STATEMENT AND BRANCH COVERAGE */

        Set a = new Set();
        Set b = new Set();

        a.section(b);
        assertTrue(Arrays.equals(new int[]{}, a.toArray()));

        a.insert(0);
        b.insert(0);
        a.section(b);
        assertTrue(Arrays.equals(new int[]{}, a.toArray()));

        a.insert(1);
        a.section(b);
        b.section(a);

        assertTrue(Arrays.equals(new int[]{1}, a.toArray()));
    }

    @Test
    public void containsArithTriple() throws Exception {

        /* STATEMENT AND BRANCH COVERAGE */
        Set set = new Set();
        int x = 1;
        int y = 2;
        int z = 3;
        set.insert(x);
        set.insert(y);
        assertTrue(!set.containsArithTriple());
        set.insert(z);
        assertTrue(set.containsArithTriple());
    }
}